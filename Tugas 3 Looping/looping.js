// No. 1 Looping While
console.log("LOOPING PERTAMA");
let firstNum = 2;
while (firstNum <= 20) {
  console.log(firstNum + " - I love coding");
  firstNum += 2;
}

console.log("LOOPING KEDUA");
let secondNum = 20;
while (secondNum >= 2) {
  console.log(secondNum + " - I will become mobile developer");
  secondNum -= 2;
}
console.log("======================================================");

// No. 2 Looping menggunakan for
for (let i = 1; i <= 20; i++) {
  if (i % 2 === 0) {
    console.log(i + "- Berkualitas");
  } else {
    if (i % 3 === 0) {
      console.log(i + "- I Love Coding");
    } else {
      console.log(i + "- Santai");
    }
  }
}
console.log("=======================================================");

// No. 3 Membuat Persegi Panjang
let vert = 4;
let horiz = 8;
for (let i = 1; i <= vert; i++) {
  let output = "";
  for (let j = 1; j <= horiz; j++) {
    output += "#";
  }
  console.log(output);
}
console.log("======================================================");

// No. 4 Membuat Tangga
let totalRows = 7;
for (let i = 1; i <= totalRows; i++) {
  let output = "";
  for (let j = 1; j <= i; j++) {
    output += "#";
  }
  console.log(output);
}
console.log("======================================================");

//No. 5 Membuat Papan Catur
let size = 8;
for (let row = 0; row < size; row++) {
  let output = "";

  for (let column = 0; column < size; column++) {
    if ((column + row) % 2 == 0) {
      output += " ";
    } else {
      output += "#";
    }
  }
  console.log(output);
}
