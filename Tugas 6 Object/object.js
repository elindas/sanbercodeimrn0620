//

function arrayToObject(arr) {
  // Code di sini
  let now = new Date();
  let thisYear = now.getFullYear();
  
  if (arr.length === 0) {
    console.log('""');
  } else {
    let obj = {};
    for (let i = 0; i < arr.length; i++) {
      obj.firstName = arr[i][0];
      obj.lastName = arr[i][1];
      obj.gender = arr[i][2];
      if (arr[i][3] === undefined || arr[i][3] > thisYear) {
        obj.age = "Invalid Birth Year";
      } else {
        obj.age = thisYear - arr[i][3];
      }
      console.log(i + 1 + ". " + obj.firstName + " " + obj.lastName);
      console.log(obj);
    }
  }
}
// Driver Code
let people = [
  ["Bruce", "Banner", "male", 1975],
  ["Natasha", "Romanoff", "female"],
];
arrayToObject(people);
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

let people2 = [
  ["Tony", "Stark", "male", 1980],
  ["Pepper", "Pots", "female", 2023],
];
arrayToObject(people2);
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case
arrayToObject([]); // ""

console.log("===============================================");
//

function shoppingTime(memberId, money) {
  if (!memberId) {
    return "Mohon maaf, toko X hanya berlaku untuk member saja";
  } else if (money < 50000 || !money) {
    return "Mohon maaf, uang tidak cukup";
  } else {
    let obj = {
      memberId: memberId,
      money: money,
      listPurchased: [],
      changeMoney: money,
    };

    let listPurchased = [
      ["Sepatu brand Stacattu", 1500000],
      ["Baju brand Zoro", 500000],
      ["Baju brand H&N", 250000],
      ["Sweater brand Uniklooh", 175000],
      ["Casing Handphone", 50000],
    ];

    for (i = 0; i < listPurchased.length; i++) {
      if (obj.changeMoney >= listPurchased[i][1]) {
        obj.listPurchased.push(listPurchased[i][0]);
        obj.changeMoney -= listPurchased[i][1];
      }
    }
    return obj;
  }
}

// TEST CASES
console.log(shoppingTime("1820RzKrnWn08", 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime("82Ku8Ma742", 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime("", 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime("234JdhweRxa53", 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log("======================================================");
//

function naikAngkot(arrPenumpang) {
  let rute = ["A", "B", "C", "D", "E", "F"];
  let result = [];

  for (let i = 0; i < arrPenumpang.length; i++) {
    let obj = {};

    obj.penumpang = arrPenumpang[i][0];
    obj.naikDari = arrPenumpang[i][1];
    obj.tujuan = arrPenumpang[i][2];
    obj.bayar;

    let awal = 0;
    let akhir = 0;
    for (let j = 0; j < rute.length; j++) {
      if (rute.indexOf(arrPenumpang[i][1]) === -1 || rute.indexOf(arrPenumpang[i][2]) === -1) {
        obj.bayar = "rute tidak ada";
      } else if (arrPenumpang[i][1] === arrPenumpang[i][2]) {
        obj.bayar = "keberangkatan dan tujuan tidak boleh sama";
      } else {
        awal = rute.indexOf(arrPenumpang[i][1]);
        akhir = rute.indexOf(arrPenumpang[i][2]);
        if (awal > akhir) {
          obj.bayar = "keberangkatan dan tujuan tidak tepat";
        } else {
          obj.bayar = (akhir - awal) * 2000;
        }
      }
    }
    result.push(obj);
  }
  return result;
}

//TEST CASE
console.log(
  naikAngkot([
    ["Dimitri", "B", "F"],
    ["Icha", "A", "B"],
  ])
);
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]
