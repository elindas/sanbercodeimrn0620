import React from "react";
import {
  StyleSheet,
  Text,
  Image,
  View,
  TextInput,
  TouchableOpacity,
} from "react-native";

export default class AboutScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Image
          source={require("./assets/logo.png")}
          style={{ width: 350, height: 100 }}
        />
        <View style={{ marginTop: 45 }}>
          <Text style={{ color: "#003366", fontSize: 28 }}>Login</Text>
        </View>
        <View style={styles.formContainer}>
          <Text style={styles.label}>Username/Email</Text>
          <TextInput style={styles.inputForm}></TextInput>
          <Text style={styles.label}>Password</Text>
          <TextInput style={styles.inputForm}></TextInput>
        </View>
        <View style={styles.buttonBox}>
          <TouchableOpacity style={styles.loginButton}>
            <Text style={{ color: "white", fontSize: 23 }}> Masuk </Text>
          </TouchableOpacity>
          <Text style={{ color: "#3EC6FF", fontSize: 20 }}>atau</Text>
          <TouchableOpacity style={styles.regButton}>
            <Text style={{ color: "white", fontSize: 23 }}> Daftar ? </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
  },

  formContainer: {
    paddingTop: 30,
  },
  label: {
    color: "#003366",
    fontSize: 14,
    padding: 5,
  },
  inputForm: {
    height: 48,
    width: 294,
    borderColor: "#003366",
    borderWidth: 1,
    backgroundColor: "#FFFFFF",
  },
  buttonBox: {
    paddingTop: 30,
    alignItems: "center",
  },
  loginButton: {
    width: 130,
    backgroundColor: "#3EC6FF",
    borderRadius: 16,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 10,
  },
  regButton: {
    width: 130,
    backgroundColor: "#003366",
    borderRadius: 16,
    height: 40,
    marginTop: 10,
    alignItems: "center",
    justifyContent: "center",
  },
});
