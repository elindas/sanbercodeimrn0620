import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { FontAwesome, Ionicons } from "@expo/vector-icons";

export default class AboutScreen extends React.Component {
  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={styles.box}>
          <Text
            style={{
              marginTop: 15,
              fontSize: 30,
              color: "#003366",
              marginTop: 80,
              fontWeight: "bold",
            }}
          >
            Tentang Saya
          </Text>

          <FontAwesome
            style={styles.iconItem}
            name="user-circle"
            size={200}
            color="#EFEFEF"
          />
          <Text
            style={{
              color: "#003366",
              fontSize: 24,
              fontWeight: "bold",
              marginTop: 25,
            }}
          >
            Elinda S
          </Text>
          <Text
            style={{
              color: "#3EC6FF",
              fontSize: 16,
              fontWeight: "bold",
              margin: 5,
            }}
          >
            React Native Developer
          </Text>

          <View style={styles.portoBox}>
            <Text
              style={{
                color: "#003366",
                fontSize: 18,
                marginTop: 4,
                marginLeft: 10,
              }}
            >
              Portofolio
            </Text>
            <View
              style={{
                height: 1,
                backgroundColor: "#003366",
                width: 343,
                alignSelf: "center",
              }}
            />
            <View style={styles.portoList}>
              <TouchableOpacity style={{ padding: 10, alignItems: "center" }}>
                <FontAwesome name="gitlab" size={42} color="#3EC6FF" />
                <Text
                  style={{
                    color: "#003366",
                    fontSize: 16,
                    fontWeight: "bold",
                  }}
                >
                  @elinda
                </Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ padding: 10, alignItems: "center" }}>
                <Ionicons name="logo-github" size={42} color="#3EC6FF" />
                <Text
                  style={{
                    color: "#003366",
                    fontSize: 16,
                    fontWeight: "bold",
                  }}
                >
                  @elinda
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.contactBox}>
            <Text
              style={{
                color: "#003366",
                fontSize: 18,
                marginTop: 3,
                marginLeft: 10,
              }}
            >
              Hubungi Saya
            </Text>
            <View
              style={{
                height: 1,
                backgroundColor: "#003366",
                width: 343,
                alignSelf: "center",
              }}
            />

            <View style={styles.socmedList}>
              <View style={styles.socAccount}>
                <FontAwesome
                  name="facebook-official"
                  size={39}
                  color="#3EC6FF"
                />
                <Text
                  style={{
                    color: "#003366",
                    fontSize: 16,
                    fontWeight: "bold",
                    margin: 10,
                  }}
                >
                  elinda_s
                </Text>
              </View>
              <View style={styles.socAccount}>
                <FontAwesome name="instagram" size={39} color="#3EC6FF" />
                <Text
                  style={{
                    color: "#003366",
                    fontSize: 16,
                    fontWeight: "bold",
                    margin: 10,
                  }}
                >
                  elinda_s
                </Text>
              </View>
              <View style={styles.socAccount}>
                <FontAwesome name="telegram" size={39} color="#3EC6FF" />
                <Text
                  style={{
                    color: "#003366",
                    fontSize: 16,
                    fontWeight: "bold",
                    margin: 10,
                  }}
                >
                  @elinda_s
                </Text>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  box: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
    display: "flex",
    marginBottom: 50,
  },
  topBar: {
    backgroundColor: "white",
    alignItems: "center",
  },
  textPage: {
    color: "#003366",
    fontSize: 36,
    fontWeight: "bold",
  },
  iconItem: {
    marginTop: 15,
  },
  portoBox: {
    display: "flex",
    backgroundColor: "#EFEFEF",
    height: 115,
    width: 350,
    marginBottom: 10,
    borderRadius: 10,
    marginTop: 10,
  },
  portoList: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
  },
  contactBox: {
    backgroundColor: "#EFEFEF",
    height: 275,
    width: 350,
    borderRadius: 20,
  },
  socmedList: {
    display: "flex",
    alignItems: "center",
  },
  socAccount: {
    display: "flex",
    flexDirection: "row",
    marginVertical: 20,
  },
});
