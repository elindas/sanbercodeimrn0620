// Soal No. 1 (Range)
function range(startNum, finishNum) {
  if (!startNum || !finishNum) {
    return -1;
  }

  let result = [];
  if (startNum < finishNum) {
    
    while (startNum <= finishNum) {
      result.push(startNum);
      startNum++;
    }

  } else if (startNum > finishNum) {
    
    while (startNum >= finishNum) {
      result.push(startNum);
      startNum--;
    }
  }
  return result;
}

console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11, 18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()); // -1

console.log("=================================================");
// Soal No. 2 (Range with Step)
function rangeWithStep(startNum, finishNum, step) {
  if (!startNum || !finishNum) {
    return -1;
  }

  let result = [];
  if (startNum < finishNum) {
    while (startNum <= finishNum) {
      result.push(startNum);
      startNum += step;
    }
  } else if (startNum > finishNum) {
    while (startNum >= finishNum) {
      result.push(startNum);
      startNum -= step;
    }
  }

  return result;
}

console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5]

console.log("=================================================");

//Soal No. 3 (Sum of Range)
function sum(startNum, finishNum, step) {
  if (!startNum) {
    return 0;
  } else if (!finishNum) {
    return startNum;
  }

  let arrayNum = [];
  if (!step) {
    arrayNum = range(startNum, finishNum);
  } else {
    arrayNum = rangeWithStep(startNum, finishNum, step);
  }
  return arrayNum.reduce((a, b) => a + b);
}

console.log(sum(1, 10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15, 10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0

console.log("=================================================");

// Soal No. 4 (Array Multidimensi)
var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
];

function dataHandling(argumen) {
  for (var i = 0; i < argumen.length; i++) {
    console.log("Nomor ID: " + argumen[i][0]);
    console.log("Nama Lengkap: " + argumen[i][1]);
    console.log("TTL: " + argumen[i][2] + " " + argumen[i][3]);
    console.log("Hobi: " + argumen[i][4] + "\n");
  }
}
dataHandling(input);
console.log("=================================================");

// Soal No. 5 (Balik Kata)

function balikKata(kata) {
  let result = "";
  for (let i = kata.length - 1; i >= 0; i--) {
    result += kata[i];
  }
  return result;
}

console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I
console.log("=================================================");

// Soal No. 6 (Metode Array)

var input = [
  "0001",
  "Roman Alamsyah ",
  "Bandar Lampung",
  "21/05/1989",
  "Membaca",
];

function dataHandling2(input) {
  // Output 1
  input.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
  input.splice(4, 1, "Pria", "SMA Internasional Metro");
  console.log(input);

  // Output 2
  let dateSplit = input[3].split("/");
  switch (dateSplit[1]) {
    case "01":
      console.log("Januari");
      break;
    case "02":
      console.log("Februari");
      break;
    case "03":
      console.log("Maret");
      break;
    case "04":
      console.log("April");
      break;
    case "05":
      console.log("Mei");
      break;
    case "06":
      console.log("Juni");
      break;
    case "07":
      console.log("Juli");
      break;
    case "08":
      console.log("Agustus");
      break;
    case "09":
      console.log("September");
      break;
    case "10":
      console.log("Oktober");
      break;
    case "11":
      console.log("November");
      break;
    case "12":
      console.log("Desember");
      break;
    default:
      console.log(" Bulan Invalid ");
      break;
  }

  // Output 3
  dateSort = dateSplit.sort(function (a, b) {
    return b - a;
  });
  console.log(dateSort);

  // Output 4
  let joinDate = input[3].split("/").join("-");
  console.log(joinDate);
  // Output 5
  let name = input[1].slice(0, 14);
  console.log(name);
}

dataHandling2(input);
